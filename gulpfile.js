'use strict';
const gulp = require("gulp");
const babel = require("gulp-babel");
const fs = require("fs");
const path = require("path");
let _nodemon;

function removeDir(dir) {
    try {
        let files = fs.readdirSync(dir);
        for (let i = 0; i < files.length; i++) {
            let newPath = path.join(dir, files[i]);
            let stat = fs.statSync(newPath);
            if (stat.isDirectory()) {
                removeDir(newPath);
            } else {
                fs.unlinkSync(newPath);
            }
        }
        fs.rmdirSync(dir)
    }
    catch (e) {

    }
}

gulp.task("build", async (done) => {
    removeDir(path.resolve(__dirname, "dist"));
    gulp.src("src/**")

        .pipe(babel())
        .pipe(gulp.dest("dist"));
    done();
});

gulp.task("dev", () => {
    _nodemon = require("./nodemon");
    gulp.watch("src/**", gulp.parallel('build'));
});