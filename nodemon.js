'use strict';
const nodemon = require('nodemon');
const nodemon_config = require('./nodemon.json');

class _nodemon extends nodemon {

    constructor() {
        super(nodemon_config);
        this
            .on('start', () => {
                console.error('nodemon started');
            })
            .on('crash', () => {
                console.error('script crashed for some reason');
            })
            .on('exit', () => {
                console.error('nodemon quit');
            });
    }

}

module.exports = new _nodemon();
