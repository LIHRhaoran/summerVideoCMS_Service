module.exports = {
  apps: [{
    name: 'summerVideoCMS_Service',
    script: './dist/main.js',
    max_memory_restart: "1024M",
    instances: "max",
    env: {
      NODE_ENV: 'production'
    },
    env_development: {
      NODE_ENV: 'development'
    }
  }]
};
