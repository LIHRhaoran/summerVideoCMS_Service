import braintree from "braintree";

const config = {
    port: 4000,
    cache: {
        expiresIn: 1,//秒,
        redis: {
            prefix: "nodeApp",
            port: 6379,
            host: '127.0.0.1',
            password: ""
        }
    }
};

export default config;