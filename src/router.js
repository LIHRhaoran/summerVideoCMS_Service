import glob from "glob"

class router {
    constructor() {
        this.glob = glob;
        this.routers = [];
        return this._getRoutes();
    }

    _getRoutes() {
        //加载routers下面所有路由
        return new Promise((resolve, reject) => {
            this.glob(__dirname + "/routers/**/router.js", async (err, files) => {

                let model = [];
                for (let i of files) {
                    model.push(import(i))
                }

                Promise.all(model).then(async res => {
                    let res2 = [];
                    for (let i of res) {
                        i=i.default.middleware();
                        res2.push(i)
                    }
                    this.routers = res2;
                    resolve(this.routers)
                });

            });
        })
    }
}

export default new router();