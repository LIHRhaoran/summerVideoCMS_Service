import glob from "glob"
import path from "path"
import _config from "./config/common"

class config {
    constructor() {
        this.glob = glob;
        this.path = path;
        this.config = _config;
        return this._getConfig()
    }

    _getConfig() {
        !process.env.NODE_ENV ? process.env.NODE_ENV = "production" : "";
        return new Promise((resolve, reject) => {
            this.glob(__dirname + "/config/**.js", async (err, files) => {
                for (let i of files) {
                    if (this.path.basename(i, ".js") === process.env.NODE_ENV) {
                        return import(i).then(async model => {
                            Object.assign(this.config, model.default);
                            resolve(this.config)
                        });
                    }
                }
            });
        })
    }
}

export default new config();