import base_routers from "../base_routers";

export default async (ctx, next) => {
    ctx.clientData = base_routers.model.user.findOne({where: ctx.request.body}).then(res => {
        if (res && res.dataValues) {
            base_routers.model.user.update({loginAt: new Date()}, {where: {id: res.dataValues.id}});
            res = Object.assign(res.dataValues, {
                token: base_routers.jwt.sign(res.dataValues, base_routers.config.jwt.secret, base_routers.config.jwt.options),
                expiresIn: base_routers.config.jwt.options.expiresIn
            });
        } else {
            throw new base_routers.exception(20000)
        }
        return res
    });

    await next();
}