import base_routers from "../base_routers";

export default async (ctx, next) => {
    ctx.clientData = base_routers.model.user
        .findOne({
            where: {
                id: ctx.state.user.id,
                password: ctx.request.body.old_password
            }
        })
        .then(async (user) => {
            if (!user) {
                throw new base_routers.exception(20002)
            } else {
                return user
                    .update(ctx.request.body)
                    .then(async (new_user) => {

                        delete new_user.dataValues.password;
                        return new_user

                    });
            }

        });

    await next();
}