import base_routers from "../base_routers";

export default async (ctx, next) => {
    ctx.clientData = base_routers.model.user
        .findOrCreate({
            where: {
                [base_routers.Op.or]: [
                    {
                        phoneNumber: ctx.request.body.phoneNumber
                    },
                    {
                        email: ctx.request.body.email
                    }
                ]
            },
            defaults: ctx.request.body
        })
        .spread(async (user, created) => {

            if (!created) {
                throw new base_routers.exception(20001)
            } else {
                delete user.dataValues.password;
                return user
            }

        });

    await next();
}