import router from "koa-joi-router"
import exception from "./../exception"
import model from "./../model"
import jwt from "jsonwebtoken"
import config from "./../config"
import Sequelize from "sequelize"
import operation_orm from "../util/operation_orm";
import braintree from "braintree"
import forge from "node-forge"
import axios from "axios"

const Joi = router.Joi;

class base_routers {
    constructor() {
        this.router = router;
        this.Joi = Joi;
        this.exception = exception;
        this.model = model;
        this.jwt = jwt;
        this.config = config;
        this.Op = Sequelize.Op;
        this.operation_orm = new operation_orm();
        this.braintree = braintree;
        this.forge = forge;
        this.axios = axios;
        this.init();
    }

    async init() {
        this.config = await config;
        this.braintree = braintree.connect(this.config.payment.braintree);
        this.braintree_clientToken = this.braintree.clientToken.generate({})
    }
}

export default new base_routers();