import base_routers from "../base_routers"

const router = base_routers.router();
const Joi = base_routers.Joi;

const routes = [
    {
        method: 'get',
        path: '/comment/list',
        validate: {
            query: Joi.object()
                .keys({
                    videoId: Joi.string(),
                }).or("videoId"),
        },
        handler: [
            require("./list").default
        ]
    },
    {
        method: 'put',
        path: '/comment/add',
        validate: {
            body: Joi.object()
                .keys({
                    videoId: Joi.string().required(),
                    content: Joi.string().required(),
                }),
            type: "json"
        },
        handler: [
            require("./add").default
        ]
    },
    {
        method: 'put',
        path: '/comment/reply',
        validate: {
            body: Joi.object()
                .keys({
                    commentId: Joi.string().required(),
                    content: Joi.string().required(),
                }),
            type: "json"
        },
        handler: [
            require("./reply").default
        ]
    },
];

export default router.route(routes)