import base_routers from "../base_routers";

export default async (ctx, next) => {
    ctx.request.body.userId = ctx.state.user.id;
    ctx.clientData = base_routers.model.comment.create(ctx.request.body);
    await next();
}