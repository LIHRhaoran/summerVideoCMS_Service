import base_routers from "../base_routers";

export default async (ctx, next) => {
    ctx.clientData = base_routers.model.comment.findAndCountAll({
        where: ctx.request.body,
        include: [
            {
                model: base_routers.model.user,
                attributes: {
                    exclude: ['email', 'phoneNumber', 'trueName', 'password']
                }
            },
            {
                model: base_routers.model.comment_reply
            }
        ]
    });
    await next();
}