import base_routers from "../base_routers"

const router = base_routers.router();
const Joi = base_routers.Joi;

const routes = [
    {
        method: 'get',
        path: '/payment_history/list',
        handler: [
            require("./list").default
        ]
    }
];

export default router.route(routes)