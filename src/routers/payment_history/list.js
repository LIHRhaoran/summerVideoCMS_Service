import base_routers from "../base_routers";

export default async (ctx, next) => {
    ctx.clientData = base_routers.model.payment_history.findAndCountAll(ctx.request.body);
    await next();
}