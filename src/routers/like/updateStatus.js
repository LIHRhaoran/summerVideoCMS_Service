import base_routers from "../base_routers";

export default async (ctx, next) => {
    ctx.request.body.userId = ctx.state.user.id;
    const like = base_routers.model.like.findCreateFind({where: ctx.request.body, defaults: ctx.request.body});
    const like_dislike = base_routers.model.like_dislike.findOne({where: ctx.request.body});
    ctx.clientData = Promise.all([like, like_dislike]).then(async ([_like, _like_dislike]) => {
        if (!_like[1]) {
            base_routers.model.like.destroy({where: ctx.request.body})
        }
        if (_like_dislike) {
            base_routers.model.like_dislike.destroy({where: ctx.request.body})
        }
        return _like[1]
    });
    await next();
}