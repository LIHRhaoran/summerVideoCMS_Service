import base_routers from "../base_routers"

const router = base_routers.router();
const Joi = base_routers.Joi;

const routes = [
    {
        method: 'get',
        path: '/like/count',
        validate: {
            query: Joi.object()
                .keys({
                    commentId: Joi.array().items(Joi.string()),
                    commentReplyId: Joi.array().items(Joi.string()),
                    videoId: Joi.array().items(Joi.string()),
                }).or("commentId", "commentReplyId", "videoId"),
        },
        handler: [
            require("./count").default
        ]
    },
    {
        method: 'get',
        path: '/like/status',
        validate: {
            query: Joi.object()
                .keys({
                    commentId: Joi.array().items(Joi.string()),
                    commentReplyId: Joi.array().items(Joi.string()),
                    videoId: Joi.array().items(Joi.string()),
                }).or("commentId", "commentReplyId", "videoId"),
        },
        handler: [
            require("./status").default
        ]
    },
    {
        method: 'post',
        path: '/like/updateStatus',
        validate: {
            body: Joi.object()
                .keys({
                    commentId: Joi.string(),
                    commentReplyId: Joi.string(),
                    videoId: Joi.string(),
                }).or("commentId", "commentReplyId", "videoId"),
            type: "json"
        },
        handler: [
            require("./updateStatus").default
        ]
    },
];

export default router.route(routes)