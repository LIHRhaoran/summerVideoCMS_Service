import base_routers from "../base_routers";

export default async (ctx, next) => {
    ctx.clientData = base_routers.model.category_1
        .findAll({where: ctx.request.body, include: {all: true}});
    await next();
}