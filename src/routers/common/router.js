import base_routers from "../base_routers"

const router = base_routers.router();
const Joi = base_routers.Joi;

const routes = [
    {
        method: 'get',
        path: '/',
        handler: [
            require("./index").default
        ]
    },
    {
        method: 'get',
        path: '/common/getCategory',
        validate: {
            query: Joi.object()
                .keys({
                    type: Joi.string(),
                }),
        },
        handler: [
            require("./getCategory").default
        ]
    },
];

export default router.route(routes)