import base_routers from "../base_routers";

export default async (ctx, next) => {
    ctx.clientData = "hello world";
    await next();
}