import base_routers from "../../base_routers";

export default async (ctx, next) => {

    ctx.clientData = {success: true};
    base_routers.braintree.transaction
        .sale({
            amount: 0.1,
            paymentMethodNonce: ctx.request.body.paymentMethodNonce,
            options: {
                submitForSettlement: true
            }
        })
        .then((err, result) => {
            console.log("resultresultresult", result);
            console.log("errerrerr", err)
        });

    await next();
}