import base_routers from "../../base_routers";

export default async (ctx, next) => {

    ctx.clientData = base_routers.braintree_clientToken;

    await next();
}