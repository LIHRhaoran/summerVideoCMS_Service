import base_routers from "../base_routers";

export default async (ctx, next) => {
    const body = ctx.request.body.commentId || ctx.request.body.commentReplyId || ctx.request.body.videoId;
    let result = [];
    for (let i = 0, len = body.length; i < len; i++) {
        result.push(
            base_routers.model.like_dislike.count({
                where: {
                    [base_routers.Op.or]: [
                        {
                            commentId: body[i]
                        },
                        {
                            commentReplyId: body[i]
                        },
                        {
                            videoId: body[i]
                        }
                    ]
                }
            })
        )
    }
    ctx.clientData = Promise.all(result)
    await next();
}