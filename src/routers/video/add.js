import base_routers from "../base_routers";

export default async (ctx, next) => {
    ctx.request.body.userId = ctx.user.id;
    ctx.clientData = base_routers.model.video
        .findOrCreate({
            where: {
                userId: ctx.request.body.userId,
                [base_routers.Op.or]: [
                    {
                        src: ctx.request.body.src
                    },
                    {
                        previewImg: ctx.request.body.previewImg
                    }
                ]
            },
            defaults: ctx.request.body
        })
        .then(async (video, created) => {
            if (!created) {
                throw new base_routers.exception(30002)
            } else {
                return video
            }
        });

    await next();
}