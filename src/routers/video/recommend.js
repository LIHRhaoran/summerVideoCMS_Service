import base_routers from "../base_routers";

export default async (ctx, next) => {

    ctx.request.body[base_routers.Op.or] = {
        title: {
            [base_routers.Op.regexp]: ctx.request.body.keyword
        },
        description: {
            [base_routers.Op.regexp]: ctx.request.body.keyword
        },
        userId: {
            [base_routers.Op.adjacent]: ctx.request.body.videoData.userId
        },
        category1Id: {
            [base_routers.Op.adjacent]: ctx.request.body.videoData.category1Id
        },
        category2Id: {
            [base_routers.Op.adjacent]: ctx.request.body.videoData.category2Id
        },
        category3Id: {
            [base_routers.Op.adjacent]: ctx.request.body.videoData.category3Id
        }
    };

    delete ctx.request.body.keyword;
    ctx.clientData = base_routers.model.video.findAndCountAll({
        where: ctx.request.body,
        include: [
            {
                model: base_routers.model.user,
                attributes: {
                    exclude: ['email', 'phoneNumber', 'trueName', 'password']
                }
            }
        ]
    });
    await next();
}