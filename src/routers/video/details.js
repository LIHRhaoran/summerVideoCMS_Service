import base_routers from "../base_routers";

export default async (ctx, next) => {
    ctx.clientData = base_routers.model.video.findOne({
        where: ctx.request.body,
        include: [
            {
                model: base_routers.model.user,
                attributes: {
                    exclude: ['email', 'phoneNumber', 'trueName', 'password']
                }
            }
        ]
    });
    await next();
}