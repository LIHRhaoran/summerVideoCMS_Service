import base_routers from "../base_routers"

const router = base_routers.router();
const Joi = base_routers.Joi;

const routes = [
    {
        method: 'get',
        path: '/video/list',
        validate: {
            query: Joi.object()
                .keys({
                    category1Id: Joi.string(),
                    category2Id: Joi.string(),
                    category3Id: Joi.string(),
                    keyword: Joi.string()
                }),
        },
        handler: [
            require("./list").default
        ]
    },
    // {
    //     method: 'get',
    //     path: '/video/details',
    //     validate: {
    //         query: Joi.object()
    //             .keys({
    //                 id: Joi.string().required(),
    //             }),
    //     },
    //     handler: [
    //         require("./details").default
    //     ]
    // },
    {
        method: 'get',
        path: '/video/recommend',
        validate: {
            query: Joi.object()
                .keys({
                    category1Id: Joi.string(),
                    category2Id: Joi.string(),
                    category3Id: Joi.string(),
                    keyword: Joi.string().required(),
                    videoData: Joi.object().required(),
                }),
        },
        handler: [
            require("./recommend").default
        ]
    },
    // {
    //     method: 'post',
    //     path: '/video/add',
    //     validate: {
    //         body: Joi.object()
    //             .keys({
    //                 title: Joi.string().required(),
    //                 src: Joi.string().uri({
    //                     scheme: [
    //                         "http",
    //                         "https",
    //                     ]
    //                 }).required(),
    //                 previewImg: Joi.string().uri({
    //                     scheme: [
    //                         "http",
    //                         "https",
    //                     ]
    //                 }).required(),
    //                 description: Joi.string(),
    //                 current_price: Joi.number().precision(2),
    //                 original_price: Joi.number().precision(2).less(Joi.ref('current_price'))
    //             }),
    //     },
    //     handler: [
    //         require("./add").default
    //     ]
    // },
];

export default router.route(routes)