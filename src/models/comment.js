import Sequelize from "sequelize"
import model from "../model"
import operation_orm from "../util/operation_orm";

let table = model.sequelize.define('comment', {
        id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            primaryKey: true,
            allowNull: false,
        },
        content: {
            type: Sequelize.STRING
        }
    });


table.associate = (sequelize) => {
    sequelize.model("comment").hasMany(sequelize.model("comment_reply"));
    sequelize.model("comment").hasMany(sequelize.model("like"));
    sequelize.model("comment").hasMany(sequelize.model("like_dislike"));
    sequelize.model("comment").belongsTo(sequelize.model("user"));
    sequelize.model("comment").belongsTo(sequelize.model("video"));
};

export default table
