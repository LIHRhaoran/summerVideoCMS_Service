import Sequelize from "sequelize"
import model from "../model"
import operation_orm from "../util/operation_orm";

let table = model.sequelize.define('like_dislike', {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
    }
});


table.associate = (sequelize) => {
    sequelize.model("like_dislike").belongsTo(sequelize.model("video"));
    sequelize.model("like_dislike").belongsTo(sequelize.model("user"));
    sequelize.model("like_dislike").belongsTo(sequelize.model("comment"));
    sequelize.model("like_dislike").belongsTo(sequelize.model("comment_reply"));
};

export default table
