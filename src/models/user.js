import Sequelize from "sequelize"
import model from "../model"

const table = model.sequelize.define('user', {
        id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            primaryKey: true,
            allowNull: false,
        },
        userName: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        trueName: {
            type: Sequelize.STRING,
        },
        email: {
            type: Sequelize.STRING,
        },
        phoneNumber: {
            type: Sequelize.STRING,
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        avatar: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        sex: {
            type: Sequelize.ENUM,
            values: ["0", "1"]
        },
        loginAt: {
            type: Sequelize.DATE,
            defaultValue: Sequelize.NOW,
            allowNull: false,
        }
    },
    {
        defaultScope: {
            attributes: {
                exclude: ['password']
            }
        }
    }
);

export default table
