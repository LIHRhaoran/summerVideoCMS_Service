import Sequelize from "sequelize"
import model from "../model"
import operation_orm from "../util/operation_orm";

let table = model.sequelize.define('views', {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
    },
    fingerprint_id: {
        type: Sequelize.STRING,
        allowNull: false,
    }
});


table.associate = (sequelize) => {
    sequelize.model("views").belongsTo(sequelize.model("video"));
};

export default table
