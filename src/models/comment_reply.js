import Sequelize from "sequelize"
import model from "../model"
import operation_orm from "../util/operation_orm";

let table = model.sequelize.define('comment_reply', {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
    },
    content: {
        type: Sequelize.STRING
    }
});


table.associate = (sequelize) => {
    sequelize.model("comment_reply").hasMany(sequelize.model("like"));
    sequelize.model("comment_reply").hasMany(sequelize.model("like_dislike"));
    sequelize.model("comment_reply").belongsTo(sequelize.model("user"));
    sequelize.model("comment_reply").belongsTo(sequelize.model("comment"));
};

export default table
