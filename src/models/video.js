import Sequelize from "sequelize"
import model from "../model"
import operation_orm from "../util/operation_orm";

let table = model.sequelize.define('video', {
        id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            primaryKey: true,
            allowNull: false,
        },
        title: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        description: {
            type: Sequelize.STRING
        },
        src: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        previewImg: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        current_price: {
            type: Sequelize.FLOAT,
        },
        original_price: {
            type: Sequelize.FLOAT,
        }
    }
);


table.associate = (sequelize) => {
    sequelize.model("video").hasMany(sequelize.model("comment"));
    sequelize.model("video").hasMany(sequelize.model("like"));
    sequelize.model("video").hasMany(sequelize.model("like_dislike"));
    sequelize.model("video").hasMany(sequelize.model("views"));
    sequelize.model("video").belongsTo(sequelize.model("user"));
    sequelize.model("video").belongsTo(sequelize.model("category_1"));
    sequelize.model("video").belongsTo(sequelize.model("category_2"));
    sequelize.model("video").belongsTo(sequelize.model("category_3"));
};

export default table
