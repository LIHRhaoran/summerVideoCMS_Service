import Sequelize from "sequelize"
import model from "../model"
import operation_orm from "../util/operation_orm";

let table = model.sequelize.define('category_1', {
        id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            primaryKey: true,
            allowNull: false,
        },
        title: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        description: {
            type: Sequelize.STRING
        },
        type: {
            type: Sequelize.STRING,
            allowNull: false,
        }
    },
    {
        hooks: {
            afterFind: async (options) => {
                return new operation_orm().merge_category(options)
            }
        }
    });


table.associate = (sequelize) => {
    sequelize.model("category_1").hasMany(sequelize.model("category_2"));
    sequelize.model("category_1").hasMany(sequelize.model("category_3"));
    sequelize.model("category_1").belongsTo(sequelize.model("user"));
};

export default table
