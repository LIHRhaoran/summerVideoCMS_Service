import Sequelize from "sequelize"
import model from "../model"

const table = model.sequelize.define('payment_history', {
        id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            primaryKey: true,
            allowNull: false,
        },
        orderNumber: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        type: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        serviceProvider: {
            type: Sequelize.ENUM,
            values: ["ispay"],
            allowNull: false,
        },
        channel: {
            type: Sequelize.ENUM,
            values: ["alipay", "wxpay", "qqpay", "bank_pc", "wxgzhpay"],
            defaultValue: "alipay",
        },
        subject: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        metaData: {
            type: Sequelize.STRING,
        },
        returnURL: {
            type: Sequelize.STRING,
        },
        price: {
            type: Sequelize.FLOAT,
            allowNull: false,
        },
        unit: {
            type: Sequelize.STRING,
            values: ["cny"],
            defaultValue: "CNY",
        },
        token: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        status: {
            type: Sequelize.ENUM,
            values: ["0", "1", "2"],
            defaultValue: "0",
        },
    }
);

table.associate = (sequelize) => {
    sequelize.model("payment_history").belongsTo(sequelize.model("user"));
    sequelize.model("payment_history").belongsTo(sequelize.model("video"));
};

export default table
