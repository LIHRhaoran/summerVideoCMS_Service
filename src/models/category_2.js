import Sequelize from "sequelize"
import model from "../model";

let table = model.sequelize.define('category_2', {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        allowNull: false,
    },
    title: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    description: {
        type: Sequelize.STRING
    }
});

table.associate = (sequelize) => {
    sequelize.model("category_2").belongsTo(sequelize.model("category_1"));
    sequelize.model("category_2").hasMany(sequelize.model("category_3"));
    sequelize.model("category_2").belongsTo(sequelize.model("user"));
};

export default table
