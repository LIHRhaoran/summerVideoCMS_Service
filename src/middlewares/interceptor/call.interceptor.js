import forge from "node-forge"
import config from "../../config"

export default async (ctx, next) => {
    let config2 = await config;
    // 全局捕抓关键属性自动加密
    try {
        if (ctx.request.body.password) {
            let password = forge.md[config2.secretKey.userEncryption.type].create();
            password.update(ctx.request.body.password + config2.secretKey.userEncryption.key);
            ctx.request.body.password = password.digest().toHex().toLowerCase();
        }

        if (ctx.request.body.old_password) {
            let password = forge.md[config2.secretKey.userEncryption.type].create();
            password.update(ctx.request.body.old_password + config2.secretKey.userEncryption.key);
            ctx.request.body.old_password = password.digest().toHex().toLowerCase();
        }
    } catch (e) {

    }

    await next();
}