import config from "../../config"
import cache from "../../cache"

import {promisify} from "util"

export default async (ctx, next) => {

    if (ctx.method.toLowerCase() === 'get') {
        const key = ctx.href + "#" + JSON.stringify(ctx.state.user);
        const getAsync = promisify(cache.client.get).bind(cache.client);
        const data = await getAsync(key).then(async res => JSON.parse(res));
        if (!data) {
            await next()
        } else {
            data.cached = true;
            ctx.body = data;
        }

    } else {
        await next();
    }
}