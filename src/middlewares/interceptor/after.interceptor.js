import cache from "../../cache"
import config from "../../config";

export default async (ctx) => {
    let clientData = {
        cached: false,
        data: await ctx.clientData
    };

    if (process.env.NODE_ENV === "development") {
        console.info(
            `${new Date().toLocaleString()}  [ ${ctx.request.method} ]  :  url:[ ${decodeURI(ctx.request.path)} ]  ======>  body:${JSON.stringify(ctx.request.body)}`
        );
    }

    if (ctx.method.toLowerCase() === 'get') {
        const key = ctx.href + "#" + JSON.stringify(ctx.state.user);
        cache.client.set(key, JSON.stringify(clientData), 'EX', (await config).cache.expiresIn)
    }

    ctx.body = clientData
}