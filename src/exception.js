import glob from "glob"

let errorCodeList = new Map();
let data = [];
glob(__dirname + "/exceptions/**.js", async (err, files) => {
    for (let i of files) {
        data.push(import(i))
    }
    Promise.all(data).then(res => {
        for (let i of res) {
            i = i.default;
            for (let i2 of Object.keys(i)) {
                errorCodeList.set(Number(i2), i[i2]);
            }
        }
    })
});

class exception {
    constructor(code) {
        this._errorCodeList = errorCodeList;
        this.message = null;
        this.createdTime = new Date().getTime();
        this.code = code;
        return this._initCode();
    }

    _initCode() {
        this.message = {
            code: this.code,
            message: this._errorCodeList.get(this.code),
            createdTime: this.createdTime,
        };
        return this
    }
}

export default exception