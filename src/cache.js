import config from "./config"
import redis from "redis"

class cache {
    constructor() {
        this.config = config;
        this.client = redis;
        this._initRedis();
    }

    async _initRedis() {
        this.config = await this.config;
        this.client = this.client.createClient(this.config.cache.redis);
    }
}

export default new cache()