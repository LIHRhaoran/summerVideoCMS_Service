import Koa from "koa";
import onerror from "koa-onerror"
import config from "./config"
import middlewares from "./middleware"

class main {

    constructor() {

        this.app = new Koa();
        this.middlewares = middlewares;
        this.config = config;
        this._initApp();

    }

    async _initApp() {

        this.config = await this.config;
        this._createServer();

    }

    async _createServer() {

        //捕抓error
        onerror(this.app, {
            accepts() {
                return 'json'
            },
            json(err, ctx) {
                ctx.body = err.message
            }
        });

        this.app.use(await this.middlewares);
        this.app.listen(this.config.port);
    }

}

export default new main();