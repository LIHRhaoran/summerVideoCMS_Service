class operation_orm {
    constructor() {

    }

    async merge_category(data) {
        //合并三级分类的方法
        for (let i9 = 0, len9 = data.length; i9 < len9; i9++) {
            const category_2s = data[i9].dataValues.category_2s;
            const category_3s = data[i9].dataValues.category_3s;
            if (category_2s) {
                for (let i = 0, len = category_2s.length; i < len; i++) {
                    if (category_3s) {
                        for (let i2 = 0, len2 = category_3s.length; i2 < len2; i2++) {

                            if (category_3s[i2].dataValues.category2Id === category_2s[i].dataValues.id) {
                                !category_2s[i].dataValues.categoryList ? category_2s[i].dataValues.categoryList = [] : "";
                                category_2s[i].dataValues.categoryList.push(category_3s[i2])
                            }

                        }
                    }
                }
            }
            data[i9].dataValues.categoryList = category_2s;
        }

        return data
    }
}

export default operation_orm;