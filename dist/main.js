"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _koa = require("koa");

var _koa2 = _interopRequireDefault(_koa);

var _koaOnerror = require("koa-onerror");

var _koaOnerror2 = _interopRequireDefault(_koaOnerror);

var _config = require("./config");

var _config2 = _interopRequireDefault(_config);

var _middleware = require("./middleware");

var _middleware2 = _interopRequireDefault(_middleware);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class main {

    constructor() {

        this.app = new _koa2.default();
        this.middlewares = _middleware2.default;
        this.config = _config2.default;
        this._initApp();
    }

    async _initApp() {

        this.config = await this.config;
        this._createServer();
    }

    async _createServer() {

        //捕抓error
        (0, _koaOnerror2.default)(this.app, {
            accepts() {
                return 'json';
            },
            json(err, ctx) {
                ctx.body = err.message;
            }
        });

        this.app.use((await this.middlewares));
        this.app.listen(this.config.port);
    }

}

exports.default = new main();