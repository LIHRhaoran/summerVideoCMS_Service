"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
const config = {
    port: 7758,
    db: {
        host: "127.0.0.1",
        port: 3306,
        dialect: "mysql",
        database: "summervideocms",
        username: "summervideocms",
        password: "summervideocms"
    }
};

exports.default = config;