"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _braintree = require("braintree");

var _braintree2 = _interopRequireDefault(_braintree);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const config = {
    port: 4000,
    cache: {
        expiresIn: 1, //秒,
        redis: {
            prefix: "nodeApp",
            port: 6379,
            host: '127.0.0.1',
            password: ""
        }
    }
};

exports.default = config;