"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _braintree = require("braintree");

var _braintree2 = _interopRequireDefault(_braintree);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const config = {
    secretKey: {
        userEncryption: {
            type: "sha512",
            key: "secretKey"
        }
    },
    ratelimit: { // 并发接口速度控制
        interval: { sec: 1 },
        timeWait: { ms: 100 },
        delayAfter: 200,
        max: 100
    },
    db: {
        host: "127.0.0.1",
        port: 3306,
        dialect: "mysql",
        database: "summervideocms2",
        username: "summervideocms2",
        password: "summervideocms2"
    },
    cache: {
        expiresIn: 60 * 60 * 2, //秒,
        redis: {
            prefix: "nodeApp",
            port: 6379,
            host: '127.0.0.1',
            password: ""
        }
    },
    router: {
        jwtRouteUnLessList: {
            path: ["/", "/user/login", "/user/register", /\/common/, /\/video/, /\/list/, /\/like\/count/, /\/like_dislike\/count/, '/payment/ispay/notify']
        }
    },
    jwt: {
        secret: "secret",
        options: {
            algorithm: "HS512",
            expiresIn: "7d"
        }
    },
    payment: {
        braintree: {
            environment: _braintree2.default.Environment.Sandbox,
            merchantId: "ywy2wxvk6b4drd9f",
            publicKey: "zv456yptfm7n8zgy",
            privateKey: "eb1f154a885362a58358aecc7f20c492"
        },
        ispay: {
            checkOrderUrl: "https://pay.ispay.cn/core/api/request/query",
            payId: 11528,
            payKey: "d001d32ca85fd580457d751ad39cba63"
        }
    },
    payment_history: {
        expiresIn: 7200000 //ms
    }
};

exports.default = config;