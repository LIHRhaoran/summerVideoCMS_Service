"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _config = require("./config");

var _config2 = _interopRequireDefault(_config);

var _koaCompose = require("koa-compose");

var _koaCompose2 = _interopRequireDefault(_koaCompose);

var _cors = require("@koa/cors");

var _cors2 = _interopRequireDefault(_cors);

var _koaJwt = require("koa-jwt");

var _koaJwt2 = _interopRequireDefault(_koaJwt);

var _koa2Ratelimit = require("koa2-ratelimit");

var _koa2Ratelimit2 = _interopRequireDefault(_koa2Ratelimit);

var _koaHelmet = require("koa-helmet");

var _koaHelmet2 = _interopRequireDefault(_koaHelmet);

var _koaCompress = require("koa-compress");

var _koaCompress2 = _interopRequireDefault(_koaCompress);

var _koaConditionalGet = require("koa-conditional-get");

var _koaConditionalGet2 = _interopRequireDefault(_koaConditionalGet);

var _koaEtag = require("koa-etag");

var _koaEtag2 = _interopRequireDefault(_koaEtag);

var _router = require("./router");

var _router2 = _interopRequireDefault(_router);

var _before = require("./middlewares/interceptor/before.interceptor");

var _before2 = _interopRequireDefault(_before);

var _cache = require("./middlewares/interceptor/cache.interceptor");

var _cache2 = _interopRequireDefault(_cache);

var _call = require("./middlewares/interceptor/call.interceptor");

var _call2 = _interopRequireDefault(_call);

var _after = require("./middlewares/interceptor/after.interceptor");

var _after2 = _interopRequireDefault(_after);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class middleware {
    constructor() {
        this.middlewares = [];
        this.config = _config2.default;
        this.router = _router2.default;
        this.ratelimit = _koa2Ratelimit2.default.RateLimit;
        return this.install_interceptor().then(() => this.compose());
    }

    async install_interceptor() {
        // 安装after before拦截器中间件
        this.config = await this.config;
        this.router = await this.router;
        for (let i2 of this.router) {
            for (let i of i2.router.stack) {
                i.stack.unshift(_before2.default);
                i.stack.splice(5, 0, _cache2.default);
                i.stack.splice(6, 0, _call2.default);
                i.stack.push(_after2.default);
            }
        }
    }

    async compose() {
        //合并中间件
        this.ratelimit = this.ratelimit.middleware(this.config.ratelimit);
        this.middlewares = [(0, _cors2.default)({}), (0, _koaJwt2.default)({ secret: this.config.jwt.secret }).unless(this.config.router.jwtRouteUnLessList), this.ratelimit, (0, _koaHelmet2.default)(), (0, _koaCompress2.default)(), (0, _koaConditionalGet2.default)(), (0, _koaEtag2.default)(), ...this.router];
        return (0, _koaCompose2.default)(this.middlewares);
    }
}

exports.default = new middleware();