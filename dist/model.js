"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _sequelize = require("sequelize");

var _sequelize2 = _interopRequireDefault(_sequelize);

var _glob = require("glob");

var _glob2 = _interopRequireDefault(_glob);

var _config = require("./config");

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

class model {
    constructor() {
        this.config = _config2.default;
        this.glob = _glob2.default;
        // redis配置
        this._initSequelize().then(() => this._getModels());
    }

    async _getModels() {
        this.glob(__dirname + "/models/**.js", async (err, files) => {
            let model = [];
            for (let i of files) {
                model.push(Promise.resolve().then(() => _interopRequireWildcard(require(`${i}`))));
            }
            Promise.all(model).then(res => {
                for (let i of res) {
                    this[i.default.options.name.singular] = i.default;
                    if (this[i.default.options.name.singular].associate) {
                        this[i.default.options.name.singular].associate(this.sequelize);
                    }
                }
                this.sequelize.sync({ alter: true });
            });
        });
    }

    async _initSequelize() {
        //初始化Sequelize
        this.config = await this.config;
        this.sequelize = new _sequelize2.default(this.config.db.database, this.config.db.username, this.config.db.password, {
            host: this.config.db.host,
            port: this.config.db.port,
            dialect: this.config.db.dialect,
            define: {
                timestamps: true,
                freezeTableName: true,
                version: true,
                hooks: {
                    beforeFind: async options => {
                        // 全局设置默认查询配置

                        if (!options.order) {
                            options.order = [];
                        }
                        if (!options.where) {
                            options.where = {};
                        }

                        // 默认createdAt降序
                        let findOptions = [['createdAt', 'DESC']];
                        options.order = [...findOptions, ...options.order];
                        options.order = [...new Map(options.order)];

                        //分页配置
                        if (!options.where.limit) {
                            options.where.limit = 10;
                        }
                        if (!options.where.offset) {
                            options.where.offset = 0;
                        }
                        options.limit = Number(options.where.limit);
                        options.offset = Number(options.where.offset);
                        delete options.where.limit;
                        delete options.where.offset;
                    }
                }
            }
        });
    }
}

exports.default = new model();