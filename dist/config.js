"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _glob = require("glob");

var _glob2 = _interopRequireDefault(_glob);

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

var _common = require("./config/common");

var _common2 = _interopRequireDefault(_common);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

class config {
    constructor() {
        this.glob = _glob2.default;
        this.path = _path2.default;
        this.config = _common2.default;
        return this._getConfig();
    }

    _getConfig() {
        !process.env.NODE_ENV ? process.env.NODE_ENV = "production" : "";
        return new Promise((resolve, reject) => {
            this.glob(__dirname + "/config/**.js", async (err, files) => {
                for (let i of files) {
                    if (this.path.basename(i, ".js") === process.env.NODE_ENV) {
                        return Promise.resolve().then(() => _interopRequireWildcard(require(`${i}`))).then(async model => {
                            Object.assign(this.config, model.default);
                            resolve(this.config);
                        });
                    }
                }
            });
        });
    }
}

exports.default = new config();