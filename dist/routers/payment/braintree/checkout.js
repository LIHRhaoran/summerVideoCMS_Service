"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _base_routers = require("../../base_routers");

var _base_routers2 = _interopRequireDefault(_base_routers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = async (ctx, next) => {

    ctx.clientData = { success: true };
    _base_routers2.default.braintree.transaction.sale({
        amount: 0.1,
        paymentMethodNonce: ctx.request.body.paymentMethodNonce,
        options: {
            submitForSettlement: true
        }
    }).then((err, result) => {
        console.log("resultresultresult", result);
        console.log("errerrerr", err);
    });

    await next();
};