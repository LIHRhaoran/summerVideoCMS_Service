"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _base_routers = require("../../base_routers");

var _base_routers2 = _interopRequireDefault(_base_routers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = async (ctx, next) => {

    const result = _base_routers2.default.model.payment_history.findAll({
        where: {
            orderNumber: ctx.request.body.orderNumber,
            userId: ctx.state.user.id
        }
    }).then(async _payment_history => {

        if (_payment_history.length === 0) {
            throw new _base_routers2.default.exception(40001);
        }

        return _payment_history;
    }).then(async _payment_history => {

        let _result = [];
        let _result2 = [];
        let _result3 = new Map();

        for (let i = 0, len = _payment_history.length; i < len; i++) {
            _result3.set(_payment_history[i].orderNumber, _payment_history[i]);
            if (_payment_history[i].dataValues.status === "1" || _payment_history[i].dataValues.status === "2") {
                _result.push({
                    price: _payment_history[i].dataValues.price,
                    returnURL: _payment_history[i].dataValues.returnURL,
                    subject: _payment_history[i].dataValues.subject,
                    metaData: _payment_history[i].dataValues.metaData,
                    orderNumber: _payment_history[i].orderNumber,
                    channel: _payment_history[i].dataValues.channel,
                    status: _payment_history[i].dataValues.status,
                    createdAt: _payment_history[i].dataValues.createdAt,
                    updatedAt: _payment_history[i].dataValues.updatedAt
                });
            } else {
                _result2.push(_base_routers2.default.axios.get(_base_routers2.default.config.payment.ispay.checkOrderUrl, {
                    params: {
                        payId: _base_routers2.default.config.payment.ispay.payId,
                        orderNumber: _payment_history[i].dataValues.orderNumber
                    }
                }));
            }
        }

        return _base_routers2.default.axios.all(_result2).then(async res => {
            for (let i = 0, len = res.length; i < len; i++) {
                const orderNumber = res[i].config.params.orderNumber;
                const _payment_history2 = _result3.get(orderNumber);
                res[i] = res[i].data;

                if (res[i].State === "success") {
                    _payment_history2.update({
                        status: "1"
                    });
                }

                _result.push({
                    price: _payment_history2.dataValues.price,
                    returnURL: _payment_history2.dataValues.returnURL,
                    subject: _payment_history2.dataValues.subject,
                    metaData: _payment_history2.dataValues.metaData,
                    orderNumber: _payment_history2.orderNumber,
                    channel: _payment_history2.dataValues.channel,
                    status: res[i].State === "success" ? "1" : _payment_history2.dataValues.status,
                    createdAt: _payment_history2.dataValues.createdAt,
                    updatedAt: res[i].State === "success" ? new Date().toISOString() : _payment_history2.dataValues.updatedAt
                });
            }

            return _result;
        }).catch(() => {
            throw new _base_routers2.default.exception(40002);
        });
    });

    ctx.clientData = result;

    await next();
};