'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _base_routers = require('../base_routers');

var _base_routers2 = _interopRequireDefault(_base_routers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _base_routers2.default.router();
const Joi = _base_routers2.default.Joi;

const routes = [{
    method: 'get',
    path: '/payment/braintree/token',
    handler: [require("./braintree/token").default]
}, {
    method: 'post',
    path: '/payment/ispay/token',
    validate: {
        body: Joi.object().keys({
            payChannel: Joi.any().valid(['alipay', 'wxpay', 'qqpay', 'bank_pc', 'wxgzhpay']).required(),
            attachData: Joi.string().required(),
            Return_url: Joi.string().required(),
            videoId: Joi.string().required(),
            type: Joi.string().required()
        }),
        type: "json"
    },
    handler: [require("./ispay/token").default]
}, {
    method: 'post',
    path: '/payment/ispay/checkOrder',
    validate: {
        body: Joi.object().keys({
            orderNumber: Joi.array().items(Joi.string()).required()
        }),
        type: "json"
    },
    handler: [require("./ispay/checkOrder").default]
}, {
    method: 'post',
    path: '/payment/braintree/checkout',
    validate: {
        body: Joi.object().keys({
            paymentMethodNonce: Joi.string().required(),
            amount: Joi.number().required()
        }),
        type: "json"
    },
    handler: [require("./braintree/checkout").default]
}];

exports.default = router.route(routes);