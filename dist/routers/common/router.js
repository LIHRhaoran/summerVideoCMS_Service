'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _base_routers = require('../base_routers');

var _base_routers2 = _interopRequireDefault(_base_routers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _base_routers2.default.router();
const Joi = _base_routers2.default.Joi;

const routes = [{
    method: 'get',
    path: '/',
    handler: [require("./index").default]
}, {
    method: 'get',
    path: '/common/getCategory',
    validate: {
        query: Joi.object().keys({
            type: Joi.string()
        })
    },
    handler: [require("./getCategory").default]
}];

exports.default = router.route(routes);