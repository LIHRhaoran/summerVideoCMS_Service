"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _base_routers = require("../base_routers");

var _base_routers2 = _interopRequireDefault(_base_routers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = async (ctx, next) => {
    ctx.request.body.userId = ctx.user.id;
    ctx.clientData = _base_routers2.default.model.video.findOrCreate({
        where: {
            userId: ctx.request.body.userId,
            [_base_routers2.default.Op.or]: [{
                src: ctx.request.body.src
            }, {
                previewImg: ctx.request.body.previewImg
            }]
        },
        defaults: ctx.request.body
    }).then(async (video, created) => {
        if (!created) {
            throw new _base_routers2.default.exception(30002);
        } else {
            return video;
        }
    });

    await next();
};