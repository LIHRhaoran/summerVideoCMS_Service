'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _base_routers = require('../base_routers');

var _base_routers2 = _interopRequireDefault(_base_routers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = async (ctx, next) => {

    if (ctx.request.body.keyword) {
        ctx.request.body[_base_routers2.default.Op.or] = {
            title: {
                [_base_routers2.default.Op.regexp]: ctx.request.body.keyword
            },
            description: {
                [_base_routers2.default.Op.regexp]: ctx.request.body.keyword
            }

        };
    }
    delete ctx.request.body.keyword;
    const video = _base_routers2.default.model.video.findAndCountAll({
        where: ctx.request.body,
        include: [{
            model: _base_routers2.default.model.user,
            attributes: {
                exclude: ['email', 'phoneNumber', 'trueName', 'password']
            }
        }]
    });
    const user_payment_history = _base_routers2.default.model.payment_history.findAll({
        where: {
            userId: ctx.state.user.id,
            type: "video",
            status: "1"
        }
    });
    ctx.clientData = Promise.all([video, user_payment_history]).then(([_video, _user_payment_history]) => {
        let _video_rows = _video.rows;
        for (let i = 0, len = _video_rows.length; i < len; i++) {
            if (_user_payment_history.length === 0 && _video_rows[i].dataValues.current_price > 0) {
                _video_rows[i].src = "请购买本视频";
            } else if (_user_payment_history.length !== 0 && _video_rows[i].dataValues.current_price > 0) {
                for (let i2 = 0, len2 = _user_payment_history.length; i2 < len2; i2++) {
                    if (_user_payment_history[i] && _user_payment_history[i].dataValues.videoId === _video_rows[i].id) {
                        _video_rows[i].src = "请购买本视频";
                    }
                }
            }
        }
        _video.rows = _video_rows;
        return _video;
    });
    await next();
};