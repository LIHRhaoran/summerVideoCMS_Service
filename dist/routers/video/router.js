'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _base_routers = require('../base_routers');

var _base_routers2 = _interopRequireDefault(_base_routers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _base_routers2.default.router();
const Joi = _base_routers2.default.Joi;

const routes = [{
    method: 'get',
    path: '/video/list',
    validate: {
        query: Joi.object().keys({
            category1Id: Joi.string(),
            category2Id: Joi.string(),
            category3Id: Joi.string(),
            keyword: Joi.string()
        })
    },
    handler: [require("./list").default]
},
// {
//     method: 'get',
//     path: '/video/details',
//     validate: {
//         query: Joi.object()
//             .keys({
//                 id: Joi.string().required(),
//             }),
//     },
//     handler: [
//         require("./details").default
//     ]
// },
{
    method: 'get',
    path: '/video/recommend',
    validate: {
        query: Joi.object().keys({
            category1Id: Joi.string(),
            category2Id: Joi.string(),
            category3Id: Joi.string(),
            keyword: Joi.string().required(),
            videoData: Joi.object().required()
        })
    },
    handler: [require("./recommend").default]
}];

exports.default = router.route(routes);