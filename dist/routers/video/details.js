'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _base_routers = require('../base_routers');

var _base_routers2 = _interopRequireDefault(_base_routers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = async (ctx, next) => {
    ctx.clientData = _base_routers2.default.model.video.findOne({
        where: ctx.request.body,
        include: [{
            model: _base_routers2.default.model.user,
            attributes: {
                exclude: ['email', 'phoneNumber', 'trueName', 'password']
            }
        }]
    });
    await next();
};