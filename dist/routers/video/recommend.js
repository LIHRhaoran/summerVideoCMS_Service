'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _base_routers = require('../base_routers');

var _base_routers2 = _interopRequireDefault(_base_routers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = async (ctx, next) => {

    ctx.request.body[_base_routers2.default.Op.or] = {
        title: {
            [_base_routers2.default.Op.regexp]: ctx.request.body.keyword
        },
        description: {
            [_base_routers2.default.Op.regexp]: ctx.request.body.keyword
        },
        userId: {
            [_base_routers2.default.Op.adjacent]: ctx.request.body.videoData.userId
        },
        category1Id: {
            [_base_routers2.default.Op.adjacent]: ctx.request.body.videoData.category1Id
        },
        category2Id: {
            [_base_routers2.default.Op.adjacent]: ctx.request.body.videoData.category2Id
        },
        category3Id: {
            [_base_routers2.default.Op.adjacent]: ctx.request.body.videoData.category3Id
        }
    };

    delete ctx.request.body.keyword;
    ctx.clientData = _base_routers2.default.model.video.findAndCountAll({
        where: ctx.request.body,
        include: [{
            model: _base_routers2.default.model.user,
            attributes: {
                exclude: ['email', 'phoneNumber', 'trueName', 'password']
            }
        }]
    });
    await next();
};