"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _koaJoiRouter = require("koa-joi-router");

var _koaJoiRouter2 = _interopRequireDefault(_koaJoiRouter);

var _exception = require("./../exception");

var _exception2 = _interopRequireDefault(_exception);

var _model = require("./../model");

var _model2 = _interopRequireDefault(_model);

var _jsonwebtoken = require("jsonwebtoken");

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _config = require("./../config");

var _config2 = _interopRequireDefault(_config);

var _sequelize = require("sequelize");

var _sequelize2 = _interopRequireDefault(_sequelize);

var _operation_orm = require("../util/operation_orm");

var _operation_orm2 = _interopRequireDefault(_operation_orm);

var _braintree = require("braintree");

var _braintree2 = _interopRequireDefault(_braintree);

var _nodeForge = require("node-forge");

var _nodeForge2 = _interopRequireDefault(_nodeForge);

var _axios = require("axios");

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const Joi = _koaJoiRouter2.default.Joi;

class base_routers {
    constructor() {
        this.router = _koaJoiRouter2.default;
        this.Joi = Joi;
        this.exception = _exception2.default;
        this.model = _model2.default;
        this.jwt = _jsonwebtoken2.default;
        this.config = _config2.default;
        this.Op = _sequelize2.default.Op;
        this.operation_orm = new _operation_orm2.default();
        this.braintree = _braintree2.default;
        this.forge = _nodeForge2.default;
        this.axios = _axios2.default;
        this.init();
    }

    async init() {
        this.config = await _config2.default;
        this.braintree = _braintree2.default.connect(this.config.payment.braintree);
        this.braintree_clientToken = this.braintree.clientToken.generate({});
    }
}

exports.default = new base_routers();