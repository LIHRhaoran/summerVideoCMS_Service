"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _base_routers = require("../base_routers");

var _base_routers2 = _interopRequireDefault(_base_routers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = async (ctx, next) => {
    ctx.request.body.userId = ctx.state.user.id;
    ctx.clientData = _base_routers2.default.model.comment_reply.create(ctx.request.body);
    await next();
};