'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _base_routers = require('../base_routers');

var _base_routers2 = _interopRequireDefault(_base_routers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _base_routers2.default.router();
const Joi = _base_routers2.default.Joi;

const routes = [{
    method: 'get',
    path: '/comment/list',
    validate: {
        query: Joi.object().keys({
            videoId: Joi.string()
        }).or("videoId")
    },
    handler: [require("./list").default]
}, {
    method: 'put',
    path: '/comment/add',
    validate: {
        body: Joi.object().keys({
            videoId: Joi.string().required(),
            content: Joi.string().required()
        }),
        type: "json"
    },
    handler: [require("./add").default]
}, {
    method: 'put',
    path: '/comment/reply',
    validate: {
        body: Joi.object().keys({
            commentId: Joi.string().required(),
            content: Joi.string().required()
        }),
        type: "json"
    },
    handler: [require("./reply").default]
}];

exports.default = router.route(routes);