'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _base_routers = require('../base_routers');

var _base_routers2 = _interopRequireDefault(_base_routers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _base_routers2.default.router();
const Joi = _base_routers2.default.Joi;

const routes = [{
    method: 'get',
    path: '/like/count',
    validate: {
        query: Joi.object().keys({
            commentId: Joi.array().items(Joi.string()),
            commentReplyId: Joi.array().items(Joi.string()),
            videoId: Joi.array().items(Joi.string())
        }).or("commentId", "commentReplyId", "videoId")
    },
    handler: [require("./count").default]
}, {
    method: 'get',
    path: '/like/status',
    validate: {
        query: Joi.object().keys({
            commentId: Joi.array().items(Joi.string()),
            commentReplyId: Joi.array().items(Joi.string()),
            videoId: Joi.array().items(Joi.string())
        }).or("commentId", "commentReplyId", "videoId")
    },
    handler: [require("./status").default]
}, {
    method: 'post',
    path: '/like/updateStatus',
    validate: {
        body: Joi.object().keys({
            commentId: Joi.string(),
            commentReplyId: Joi.string(),
            videoId: Joi.string()
        }).or("commentId", "commentReplyId", "videoId"),
        type: "json"
    },
    handler: [require("./updateStatus").default]
}];

exports.default = router.route(routes);