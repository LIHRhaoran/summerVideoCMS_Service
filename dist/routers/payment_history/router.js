'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _base_routers = require('../base_routers');

var _base_routers2 = _interopRequireDefault(_base_routers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _base_routers2.default.router();
const Joi = _base_routers2.default.Joi;

const routes = [{
    method: 'get',
    path: '/payment_history/list',
    handler: [require("./list").default]
}];

exports.default = router.route(routes);