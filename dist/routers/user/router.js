'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _base_routers = require('../base_routers');

var _base_routers2 = _interopRequireDefault(_base_routers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _base_routers2.default.router();
const Joi = _base_routers2.default.Joi;

const routes = [{
    method: 'post',
    path: '/user/login',
    validate: {
        body: Joi.object().keys({
            phoneNumber: Joi.string().length(11),
            email: Joi.string().email(),
            password: Joi.string().hex().required()
        }).xor("phoneNumber", "email"),
        type: "json"
    },
    handler: [require("./login").default]
}, {
    method: 'post',
    path: '/user/resetPassword',
    validate: {
        body: Joi.object().keys({
            password: Joi.string().hex().required(),
            old_password: Joi.string().hex().required()
        }),
        type: "json"
    },
    handler: [require("./resetPassword").default]
}, {
    method: 'post',
    path: '/user/setUserInfo',
    validate: {
        body: Joi.object().keys({
            userName: Joi.string(),
            avatar: Joi.string(),
            phoneNumber: Joi.string().length(11),
            email: Joi.string().email(),
            trueName: Joi.string(),
            sex: Joi.any().valid(['0', '1'])
        }),
        type: "json"
    },
    handler: [require("./setUserInfo").default]
}, {
    method: 'put',
    path: '/user/register',
    validate: {
        body: Joi.object().keys({
            userName: Joi.string().required(),
            avatar: Joi.string().required(),
            phoneNumber: Joi.string().length(11),
            email: Joi.string().email(),
            password: Joi.string().hex().required(),
            trueName: Joi.string(),
            sex: Joi.any().valid(['0', '1'])
        }).or("phoneNumber", "email"),
        type: "json"
    },
    handler: [require("./register").default]
}];

exports.default = router.route(routes);