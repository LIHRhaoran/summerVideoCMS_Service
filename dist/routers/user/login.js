"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _base_routers = require("../base_routers");

var _base_routers2 = _interopRequireDefault(_base_routers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = async (ctx, next) => {
    ctx.clientData = _base_routers2.default.model.user.findOne({ where: ctx.request.body }).then(res => {
        if (res && res.dataValues) {
            _base_routers2.default.model.user.update({ loginAt: new Date() }, { where: { id: res.dataValues.id } });
            res = Object.assign(res.dataValues, {
                token: _base_routers2.default.jwt.sign(res.dataValues, _base_routers2.default.config.jwt.secret, _base_routers2.default.config.jwt.options),
                expiresIn: _base_routers2.default.config.jwt.options.expiresIn
            });
        } else {
            throw new _base_routers2.default.exception(20000);
        }
        return res;
    });

    await next();
};