"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _base_routers = require("../base_routers");

var _base_routers2 = _interopRequireDefault(_base_routers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = async (ctx, next) => {
    ctx.clientData = _base_routers2.default.model.user.findOne({
        where: {
            id: ctx.state.user.id,
            password: ctx.request.body.old_password
        }
    }).then(async user => {
        if (!user) {
            throw new _base_routers2.default.exception(20002);
        } else {
            return user.update(ctx.request.body).then(async new_user => {

                delete new_user.dataValues.password;
                return new_user;
            });
        }
    });

    await next();
};