"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _base_routers = require("../base_routers");

var _base_routers2 = _interopRequireDefault(_base_routers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = async (ctx, next) => {
    ctx.clientData = _base_routers2.default.model.user.findOrCreate({
        where: {
            [_base_routers2.default.Op.or]: [{
                phoneNumber: ctx.request.body.phoneNumber
            }, {
                email: ctx.request.body.email
            }]
        },
        defaults: ctx.request.body
    }).spread(async (user, created) => {

        if (!created) {
            throw new _base_routers2.default.exception(20001);
        } else {
            delete user.dataValues.password;
            return user;
        }
    });

    await next();
};