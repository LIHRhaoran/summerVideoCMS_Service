"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _base_routers = require("../base_routers");

var _base_routers2 = _interopRequireDefault(_base_routers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = async (ctx, next) => {
    ctx.request.body.userId = ctx.state.user.id;
    const like = _base_routers2.default.model.like.findOne({ where: ctx.request.body });
    const like_dislike = _base_routers2.default.model.like_dislike.findCreateFind({
        where: ctx.request.body,
        defaults: ctx.request.body
    });
    ctx.clientData = Promise.all([like, like_dislike]).then(async ([_like, _like_dislike]) => {
        if (!_like_dislike[1]) {
            _base_routers2.default.model.like_dislike.destroy({ where: ctx.request.body });
        }
        if (_like) {
            _base_routers2.default.model.like.destroy({ where: ctx.request.body });
        }
        return _like_dislike[1];
    });
    await next();
};