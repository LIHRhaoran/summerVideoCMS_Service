"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _base_routers = require("../base_routers");

var _base_routers2 = _interopRequireDefault(_base_routers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = async (ctx, next) => {
    const body = ctx.request.body.commentId || ctx.request.body.commentReplyId || ctx.request.body.videoId;
    let result = [];
    for (let i = 0, len = body.length; i < len; i++) {
        result.push(_base_routers2.default.model.like.count({
            where: {
                userId: ctx.state.user.id,
                [_base_routers2.default.Op.or]: [{
                    commentId: body[i]
                }, {
                    commentReplyId: body[i]
                }, {
                    videoId: body[i]
                }]
            }
        }));
    }
    ctx.clientData = Promise.all(result);
    await next();
};