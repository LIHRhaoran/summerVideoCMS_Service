"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _glob = require("glob");

var _glob2 = _interopRequireDefault(_glob);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

let errorCodeList = new Map();
let data = [];
(0, _glob2.default)(__dirname + "/exceptions/**.js", async (err, files) => {
    for (let i of files) {
        data.push(Promise.resolve().then(() => _interopRequireWildcard(require(`${i}`))));
    }
    Promise.all(data).then(res => {
        for (let i of res) {
            i = i.default;
            for (let i2 of Object.keys(i)) {
                errorCodeList.set(Number(i2), i[i2]);
            }
        }
    });
});

class exception {
    constructor(code) {
        this._errorCodeList = errorCodeList;
        this.message = null;
        this.createdTime = new Date().getTime();
        this.code = code;
        return this._initCode();
    }

    _initCode() {
        this.message = {
            code: this.code,
            message: this._errorCodeList.get(this.code),
            createdTime: this.createdTime
        };
        return this;
    }
}

exports.default = exception;