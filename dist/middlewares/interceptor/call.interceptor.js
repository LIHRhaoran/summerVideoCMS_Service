"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _nodeForge = require("node-forge");

var _nodeForge2 = _interopRequireDefault(_nodeForge);

var _config = require("../../config");

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = async (ctx, next) => {
    let config2 = await _config2.default;
    // 全局捕抓关键属性自动加密
    try {
        if (ctx.request.body.password) {
            let password = _nodeForge2.default.md[config2.secretKey.userEncryption.type].create();
            password.update(ctx.request.body.password + config2.secretKey.userEncryption.key);
            ctx.request.body.password = password.digest().toHex().toLowerCase();
        }

        if (ctx.request.body.old_password) {
            let password = _nodeForge2.default.md[config2.secretKey.userEncryption.type].create();
            password.update(ctx.request.body.old_password + config2.secretKey.userEncryption.key);
            ctx.request.body.old_password = password.digest().toHex().toLowerCase();
        }
    } catch (e) {}

    await next();
};