"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _config = require("../../config");

var _config2 = _interopRequireDefault(_config);

var _cache = require("../../cache");

var _cache2 = _interopRequireDefault(_cache);

var _util = require("util");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = async (ctx, next) => {

    if (ctx.method.toLowerCase() === 'get') {
        const key = ctx.href + "#" + JSON.stringify(ctx.state.user);
        const getAsync = (0, _util.promisify)(_cache2.default.client.get).bind(_cache2.default.client);
        const data = await getAsync(key).then(async res => JSON.parse(res));
        if (!data) {
            await next();
        } else {
            data.cached = true;
            ctx.body = data;
        }
    } else {
        await next();
    }
};