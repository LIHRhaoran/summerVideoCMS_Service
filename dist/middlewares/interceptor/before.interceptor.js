"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = async (ctx, next) => {
    if (ctx.method.toLowerCase() === "get") {
        ctx.request.body = ctx.query;
    }
    if (!ctx.state.user) {
        ctx.state.user = {
            id: "空"
        };
    }
    await next();
};