"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _cache = require("../../cache");

var _cache2 = _interopRequireDefault(_cache);

var _config = require("../../config");

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = async ctx => {
    let clientData = {
        cached: false,
        data: await ctx.clientData
    };

    if (process.env.NODE_ENV === "development") {
        console.info(`${new Date().toLocaleString()}  [ ${ctx.request.method} ]  :  url:[ ${decodeURI(ctx.request.path)} ]  ======>  body:${JSON.stringify(ctx.request.body)}`);
    }

    if (ctx.method.toLowerCase() === 'get') {
        const key = ctx.href + "#" + JSON.stringify(ctx.state.user);
        _cache2.default.client.set(key, JSON.stringify(clientData), 'EX', (await _config2.default).cache.expiresIn);
    }

    ctx.body = clientData;
};