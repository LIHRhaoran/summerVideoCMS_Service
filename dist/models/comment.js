"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _sequelize = require("sequelize");

var _sequelize2 = _interopRequireDefault(_sequelize);

var _model = require("../model");

var _model2 = _interopRequireDefault(_model);

var _operation_orm = require("../util/operation_orm");

var _operation_orm2 = _interopRequireDefault(_operation_orm);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let table = _model2.default.sequelize.define('comment', {
    id: {
        type: _sequelize2.default.UUID,
        defaultValue: _sequelize2.default.UUIDV4,
        primaryKey: true,
        allowNull: false
    },
    content: {
        type: _sequelize2.default.STRING
    }
});

table.associate = sequelize => {
    sequelize.model("comment").hasMany(sequelize.model("comment_reply"));
    sequelize.model("comment").hasMany(sequelize.model("like"));
    sequelize.model("comment").hasMany(sequelize.model("like_dislike"));
    sequelize.model("comment").belongsTo(sequelize.model("user"));
    sequelize.model("comment").belongsTo(sequelize.model("video"));
};

exports.default = table;