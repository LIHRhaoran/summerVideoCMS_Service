"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _sequelize = require("sequelize");

var _sequelize2 = _interopRequireDefault(_sequelize);

var _model = require("../model");

var _model2 = _interopRequireDefault(_model);

var _operation_orm = require("../util/operation_orm");

var _operation_orm2 = _interopRequireDefault(_operation_orm);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let table = _model2.default.sequelize.define('video', {
    id: {
        type: _sequelize2.default.UUID,
        defaultValue: _sequelize2.default.UUIDV4,
        primaryKey: true,
        allowNull: false
    },
    title: {
        type: _sequelize2.default.STRING,
        allowNull: false
    },
    description: {
        type: _sequelize2.default.STRING
    },
    src: {
        type: _sequelize2.default.STRING,
        allowNull: false
    },
    previewImg: {
        type: _sequelize2.default.STRING,
        allowNull: false
    },
    current_price: {
        type: _sequelize2.default.FLOAT
    },
    original_price: {
        type: _sequelize2.default.FLOAT
    }
});

table.associate = sequelize => {
    sequelize.model("video").hasMany(sequelize.model("comment"));
    sequelize.model("video").hasMany(sequelize.model("like"));
    sequelize.model("video").hasMany(sequelize.model("like_dislike"));
    sequelize.model("video").hasMany(sequelize.model("views"));
    sequelize.model("video").belongsTo(sequelize.model("user"));
    sequelize.model("video").belongsTo(sequelize.model("category_1"));
    sequelize.model("video").belongsTo(sequelize.model("category_2"));
    sequelize.model("video").belongsTo(sequelize.model("category_3"));
};

exports.default = table;