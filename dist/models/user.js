"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _sequelize = require("sequelize");

var _sequelize2 = _interopRequireDefault(_sequelize);

var _model = require("../model");

var _model2 = _interopRequireDefault(_model);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const table = _model2.default.sequelize.define('user', {
    id: {
        type: _sequelize2.default.UUID,
        defaultValue: _sequelize2.default.UUIDV4,
        primaryKey: true,
        allowNull: false
    },
    userName: {
        type: _sequelize2.default.STRING,
        allowNull: false
    },
    trueName: {
        type: _sequelize2.default.STRING
    },
    email: {
        type: _sequelize2.default.STRING
    },
    phoneNumber: {
        type: _sequelize2.default.STRING
    },
    password: {
        type: _sequelize2.default.STRING,
        allowNull: false
    },
    avatar: {
        type: _sequelize2.default.STRING,
        allowNull: false
    },
    sex: {
        type: _sequelize2.default.ENUM,
        values: ["0", "1"]
    },
    loginAt: {
        type: _sequelize2.default.DATE,
        defaultValue: _sequelize2.default.NOW,
        allowNull: false
    }
}, {
    defaultScope: {
        attributes: {
            exclude: ['password']
        }
    }
});

exports.default = table;