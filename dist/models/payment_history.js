"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _sequelize = require("sequelize");

var _sequelize2 = _interopRequireDefault(_sequelize);

var _model = require("../model");

var _model2 = _interopRequireDefault(_model);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const table = _model2.default.sequelize.define('payment_history', {
    id: {
        type: _sequelize2.default.UUID,
        defaultValue: _sequelize2.default.UUIDV4,
        primaryKey: true,
        allowNull: false
    },
    orderNumber: {
        type: _sequelize2.default.STRING,
        allowNull: false
    },
    type: {
        type: _sequelize2.default.STRING,
        allowNull: false
    },
    serviceProvider: {
        type: _sequelize2.default.ENUM,
        values: ["ispay"],
        allowNull: false
    },
    channel: {
        type: _sequelize2.default.ENUM,
        values: ["alipay", "wxpay", "qqpay", "bank_pc", "wxgzhpay"],
        defaultValue: "alipay"
    },
    subject: {
        type: _sequelize2.default.STRING,
        allowNull: false
    },
    metaData: {
        type: _sequelize2.default.STRING
    },
    returnURL: {
        type: _sequelize2.default.STRING
    },
    price: {
        type: _sequelize2.default.FLOAT,
        allowNull: false
    },
    unit: {
        type: _sequelize2.default.STRING,
        values: ["cny"],
        defaultValue: "CNY"
    },
    token: {
        type: _sequelize2.default.STRING,
        allowNull: false
    },
    status: {
        type: _sequelize2.default.ENUM,
        values: ["0", "1", "2"],
        defaultValue: "0"
    }
});

table.associate = sequelize => {
    sequelize.model("payment_history").belongsTo(sequelize.model("user"));
    sequelize.model("payment_history").belongsTo(sequelize.model("video"));
};

exports.default = table;