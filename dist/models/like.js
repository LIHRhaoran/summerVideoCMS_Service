"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _sequelize = require("sequelize");

var _sequelize2 = _interopRequireDefault(_sequelize);

var _model = require("../model");

var _model2 = _interopRequireDefault(_model);

var _operation_orm = require("../util/operation_orm");

var _operation_orm2 = _interopRequireDefault(_operation_orm);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let table = _model2.default.sequelize.define('like', {
    id: {
        type: _sequelize2.default.UUID,
        defaultValue: _sequelize2.default.UUIDV4,
        primaryKey: true,
        allowNull: false
    }
});

table.associate = sequelize => {
    sequelize.model("like").belongsTo(sequelize.model("video"));
    sequelize.model("like").belongsTo(sequelize.model("user"));
    sequelize.model("like").belongsTo(sequelize.model("comment"));
    sequelize.model("like").belongsTo(sequelize.model("comment_reply"));
};

exports.default = table;