"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _sequelize = require("sequelize");

var _sequelize2 = _interopRequireDefault(_sequelize);

var _model = require("../model");

var _model2 = _interopRequireDefault(_model);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let table = _model2.default.sequelize.define('category_2', {
    id: {
        type: _sequelize2.default.UUID,
        defaultValue: _sequelize2.default.UUIDV4,
        primaryKey: true,
        allowNull: false
    },
    title: {
        type: _sequelize2.default.STRING,
        allowNull: false
    },
    description: {
        type: _sequelize2.default.STRING
    }
});

table.associate = sequelize => {
    sequelize.model("category_2").belongsTo(sequelize.model("category_1"));
    sequelize.model("category_2").hasMany(sequelize.model("category_3"));
    sequelize.model("category_2").belongsTo(sequelize.model("user"));
};

exports.default = table;