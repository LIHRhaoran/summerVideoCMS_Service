"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _glob = require("glob");

var _glob2 = _interopRequireDefault(_glob);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

class router {
    constructor() {
        this.glob = _glob2.default;
        this.routers = [];
        return this._getRoutes();
    }

    _getRoutes() {
        //加载routers下面所有路由
        return new Promise((resolve, reject) => {
            this.glob(__dirname + "/routers/**/router.js", async (err, files) => {

                let model = [];
                for (let i of files) {
                    model.push(Promise.resolve().then(() => _interopRequireWildcard(require(`${i}`))));
                }

                Promise.all(model).then(async res => {
                    let res2 = [];
                    for (let i of res) {
                        i = i.default.middleware();
                        res2.push(i);
                    }
                    this.routers = res2;
                    resolve(this.routers);
                });
            });
        });
    }
}

exports.default = new router();