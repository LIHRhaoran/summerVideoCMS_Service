"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
const code = {
    "20000": "该用户不存在",
    "20001": "该用户已存在",
    "20002": "旧密码不正确"
};
exports.default = code;