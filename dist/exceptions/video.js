"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
const code = {
    "30000": "该视频不存在",
    "30001": "该视频价格为空",
    "30002": "禁止重复上传"
};
exports.default = code;