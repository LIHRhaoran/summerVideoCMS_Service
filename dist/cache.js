"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _config = require("./config");

var _config2 = _interopRequireDefault(_config);

var _redis = require("redis");

var _redis2 = _interopRequireDefault(_redis);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class cache {
    constructor() {
        this.config = _config2.default;
        this.client = _redis2.default;
        this._initRedis();
    }

    async _initRedis() {
        this.config = await this.config;
        this.client = this.client.createClient(this.config.cache.redis);
    }
}

exports.default = new cache();